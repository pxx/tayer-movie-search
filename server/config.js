module.exports = {
    api: {
        prefix: '/api',
        port: process.env.API_PORT || 9000,
        options: {
            name: 'API'
        },
        corsOrigins: ['*'],
        cacheTTL: 60000
    },
    omdb: {
        apiBaseUrl: 'http://www.omdbapi.com/',
        apiKey: '4925030d'
    },
    searchMoviesNumber: 20
};
