const restify = require('restify');

const redisService = require('./services/redis');
const config = require('./config');

const server = restify.createServer(config.api.options);
global.server = server;

server.use(restify.plugins.queryParser());
require('./middlewares/logger');
require('./middlewares/cors');

require('./middlewares/maxAge');
require('./middlewares/redis');

redisService.connect()
    .then((client) => {
        global.redis = client;
        server.listen(config.api.port);
    });
server.on('listening', () => {
    console.info(`${server.name} is listening at ${server.url}`);

    require('./routes');
});
