const config = require('../config');
const searchRouter = require('./search/search.router');
const cacheRouter = require('./cache/cache.router');

[
    searchRouter,
    cacheRouter
].forEach((router) => {
    router.applyRoutes(global.server, config.api.prefix);
});

console.info('API routes are registered');
