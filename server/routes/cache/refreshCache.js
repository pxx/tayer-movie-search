const omdbClient = require('../../services/omdbClient');
const redisService = require('../../services/redis');

async function refreshCache(req, res, next) {
    try {
        const keys = await global.redis.keysAsync('*');
        keys.forEach(async (key) => {
            const result = await omdbClient.search(key, key);
            redisService.cache(key, result);
        });
    } catch (error) {}
    return res.send(200);
}

module.exports = refreshCache;
