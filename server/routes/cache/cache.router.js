const { Router } = require('restify-router');
const cacheRouter = new Router();

cacheRouter.post('/cache/refresh', require('./refreshCache'));

module.exports = cacheRouter;
