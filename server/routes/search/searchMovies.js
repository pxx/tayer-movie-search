const omdbClient = require('../../services/omdbClient');
const redisService = require('../../services/redis');

async function searchMovies(req, res, next) {
    try {
        const query = req.query.keyword;
        if (query) {
            const result = await omdbClient.search(query, query);
            redisService.cache(query, result);
            if (!res.finished) {
                res.send(200, result);
                console.info(`Fullfill ${req.method} ${req.url} from OMDB`);
            }
        } else {
            res.send(400, { error: 'Missing keyword' });
        }
    } catch (error) {
        res.send(400, error);
    }
    return next();
}

module.exports = searchMovies;
