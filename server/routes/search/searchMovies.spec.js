const omdbClient = require('../../services/omdbClient');
const redisService = require('../../services/redis');
const searchMovies = require('./searchMovies');

const nextFn = () => 'next';
const res = {
    send: jest.fn()
};

jest.mock('../../services/omdbClient');
jest.mock('../../services/redis');
omdbClient.search.mockImplementation(() => {
    return 'search results';
});

describe('searchMovies router', () => {
    beforeEach(() => {
        res.send.mockClear();
    });

    test('Should return nex t()', async () => {
        const result = await searchMovies({}, res, nextFn);
        expect(result).toBe('next');
    });

    test('Should send 400 if error happens', async () => {
        await searchMovies({}, res, nextFn);
        expect(res.send.mock.calls[0][0]).toBe(400);
    });

    test('Should send 400 if no keyword', async () => {
        await searchMovies({ query: { keyword: '' } }, res, nextFn);
        expect(res.send).toHaveBeenCalledWith(400, { error: 'Missing keyword' });
    });

    test('Should search for movies and store them to cache', async () => {
        await searchMovies({ query: { keyword: 'kw' } }, res, nextFn);
        expect(omdbClient.search).toHaveBeenCalledWith('kw', 'kw');
        expect(redisService.cache).toHaveBeenCalledWith('kw', 'search results');
    });

    test('Should send 200 with results if response is not finished', async () => {
        res.finished = false;
        await searchMovies({ query: { keyword: 'kw' } }, res, nextFn);

        expect(res.send).toHaveBeenCalledWith(200, 'search results');


        res.send.mockClear();
        res.finished = true;
        await searchMovies({ query: { keyword: 'kw' } }, res, nextFn);

        expect(res.send).not.toHaveBeenCalled();
    });
});
