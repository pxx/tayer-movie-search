const { Router } = require('restify-router');
const searchRouter = new Router();

searchRouter.get('/search', require('./searchMovies'));

module.exports = searchRouter;
