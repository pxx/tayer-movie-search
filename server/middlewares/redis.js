const config = require('../config');

global.server.use(async (req, res, next) => {
    if (req.method === 'GET') {
        const cacheKey = req.url;
        try {
            const cachedRaw = await global.redis.getAsync(cacheKey);
            if (cachedRaw) {
                const cached = JSON.parse(cachedRaw);
                console.info(`Fullfill ${req.method} ${req.url} from cache`);

                if (new Date().valueOf() - cached.createdAt > config.api.cacheTTL) {
                    res.send(200, cached.data);
                } else {
                    return res.send(200, cached.data);
                }
            }
        } catch (err) {}
    }
    return next();
});
