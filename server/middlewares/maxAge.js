global.server.use((req, res, next) => {
    res.cache('public', { maxAge: 30 });
    return next();
});
