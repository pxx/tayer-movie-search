global.server.pre(function (req, res, next) {
    console.info(`${req.method} ${req.url}`);
    return next();
});
