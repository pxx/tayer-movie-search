const config = require('../config');

const nextFn = jest.fn();
const getFn = jest.fn();
const sendFn = jest.fn();
const res = { send: sendFn };
let useHandler;


global.server = {
    use: (handler) => {
        useHandler = handler;
    }
};
global.redis = {
    getAsync: getFn
};

require('./redis');


describe('redis cache middleware', () => {
    beforeEach(() => {
        nextFn.mockClear();
        getFn.mockClear();
        sendFn.mockClear();
    });

    test('Should invoke caching in case of GET request', () => {
        useHandler({ method: 'GET' }, res, nextFn);
        expect(getFn).toHaveBeenCalledTimes(1);
    });

    test('Should not invoke caching in case of POST, PUT, PATCH, DELETE, OPTIONS requests', () => {
        useHandler({ method: 'POST' }, res, nextFn);
        useHandler({ method: 'PUT' }, res, nextFn);
        useHandler({ method: 'PATCH' }, res, nextFn);
        useHandler({ method: 'DELETE' }, res, nextFn);
        useHandler({ method: 'OPTIONS' }, res, nextFn);
        expect(getFn).not.toHaveBeenCalled();
    });

    test('Should search for cached data with key = req.url', () => {
        useHandler({ method: 'GET', url: 'testurl' }, res, nextFn);
        expect(getFn).toHaveBeenCalledWith('testurl');
    });

    test('Should process with cached value if any', async () => {
        const spyJsonParse = jest.spyOn(JSON, 'parse');
        getFn.mockImplementation((key) => {
            return Promise.resolve('value');
        });

        await useHandler({ method: 'GET', url: 'testurl' }, res, nextFn);
        expect(spyJsonParse).toHaveBeenCalled();
    });

    test('Should return next() if cache is old', async () => {
        const cachedRaw = JSON.stringify({ createdAt: new Date().valueOf() - config.api.cacheTTL - 1 });
        getFn.mockImplementation((key) => {
            return Promise.resolve(cachedRaw);
        });
        nextFn.mockImplementation(() => {
            return 'next';
        });

        const result = await useHandler({ method: 'GET', url: 'testurl' }, res, nextFn);
        expect(sendFn).toHaveBeenCalled();
        expect(nextFn).toHaveBeenCalled();
        expect(result).toBe('next');
    });

    test('Should return res.send() if cache is new', async () => {
        getFn.mockImplementation((key) => {
            const cachedRaw = JSON.stringify({ createdAt: new Date().valueOf() - config.api.cacheTTL + 100 });
            return Promise.resolve(cachedRaw);
        });

        sendFn.mockImplementation(() => 'send');

        const result = await useHandler({ method: 'GET', url: 'testurl' }, res, nextFn);
        expect(sendFn).toHaveBeenCalled();
        expect(nextFn).not.toHaveBeenCalled();
        expect(result).toBe('send');
    });

});
