const corsMiddleware = require('restify-cors-middleware');
const config = require('../config');

const cors = corsMiddleware({
    preflightMaxAge: 5, //Optional
    origins: config.api.corsOrigins
});
global.server.pre(cors.preflight);
global.server.use(cors.actual);
