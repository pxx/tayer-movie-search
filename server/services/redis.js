const bluebird = require('bluebird');
const redis = require('redis');
bluebird.promisifyAll(redis);

let client;
module.exports.connect = () => {
    return new Promise((resolve, reject) => {
        client = redis.createClient(process.env.REDIS_URL);
        client.on('error', function (err) {
            console.error(`Redis error ${err}`);
        });
        client.on('connect', function () {
            console.info('Connected to redis instance');
            // client.flushall();
            resolve(client);
        });
    });
};

module.exports.cache = (key, value) => {
    const payload = {
        createdAt: new Date().valueOf(),
        data: value
    };
    client.set(key, JSON.stringify(payload));
};
