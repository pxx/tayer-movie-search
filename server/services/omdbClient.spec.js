const mockAxios = require('axios');
const { search } = require('./omdbClient');

jest.mock('../config', () => ({
    omdb: { apiKey: 'key' },
    searchMoviesNumber: 20
}));

describe('omdbClient', () => {
    beforeEach(() => {
        mockAxios.get.mockClear();
    });

    test('Should fetch data from external service once', async () => {
        mockAxios.get.mockImplementation(() => generateData(10, 10));
        await search('kw');
        expect(mockAxios.get).toHaveBeenCalledTimes(1);
    });

    test('Should fetch data from external service twice', async () => {
        mockAxios.get.mockImplementation(() => generateData(10, 100));
        await search('kw');
        expect(mockAxios.get).toHaveBeenCalledTimes(2);
    });

    test('Should return exact number of movies', async () => {
        mockAxios.get.mockImplementation(() => generateData(15, 100));
        const results = await search('kw');
        expect(results.length).toBe(20);
    });

    test('Should reject in case of internal error, e.g. network', () => {
        mockAxios.get.mockImplementation(() => Promise.reject('network error'));
        expect(search('kw')).rejects.toEqual('network error');
    });
});

function generateData(items, total) {
    return {
        data: {
            Response: 'True',
            totalResults: total,
            Search: Array(items).fill({})
        }
    };
}
