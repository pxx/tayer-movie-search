const requests = {};

module.exports.concurrencyWrapper = (promise) => {
    return (cacheKey, ...args) => {
        if (requests[cacheKey]) {
            return requests[cacheKey];
        } else {
            requests[cacheKey] = promise(...args);
            requests[cacheKey]
                .then(() => {
                    delete requests[cacheKey];
                })
                .catch(() => {
                    delete requests[cacheKey];
                });
            return requests[cacheKey];
        }
    };
};
