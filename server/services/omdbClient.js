const axios = require('axios');

const config = require('../config');
const { concurrencyWrapper } = require('../services/utils');

module.exports.search = concurrencyWrapper(async (query) => {
    const movies = [];
    const params = {
        apikey: config.omdb.apiKey,
        s: query,
        page: 1
    };

    try {
        let total = Infinity;
        while (
            movies.length < config.searchMoviesNumber
            && movies.length < total
        ) {
            const moviesPage = await axios.get(config.omdb.apiBaseUrl, { params });
            console.info(`OMDB GET ${config.omdb.apiBaseUrl} ${JSON.stringify(params)}`);
            if (moviesPage.data.Response === 'True') {
                movies.splice(0, 0, ...mapMovies(moviesPage.data.Search));
                total = moviesPage.data.totalResults;
                params.page++;
            } else {
                total = 0;
            }
        }

        if (movies.length > config.searchMoviesNumber) {
            movies.length = config.searchMoviesNumber;
        }

        return movies;
    } catch (error) {
        throw new Error(error);
    }
});


function mapMovies(movies) {
    return movies.map((movie) => ({
        title: movie.Title,
        poster: movie.Poster === 'N/A' ? null : movie.Poster
    }));
}
