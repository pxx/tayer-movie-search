module.exports = {
    verbose: true,
    rootDir: './server',
    roots: ['<rootDir>', '<rootDir>/../testing'],
    collectCoverage: true,
    coverageDirectory: '<rootDir>/../coverage/server',
    collectCoverageFrom: [
        '<rootDir>/**/*.js',
        '!**/__mocks__/**'
    ]
};
