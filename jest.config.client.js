module.exports = {
    verbose: true,
    rootDir: './client',
    roots: ['<rootDir>', '<rootDir>/../testing'],
    collectCoverage: true,
    coverageDirectory: '<rootDir>/../coverage/client',
    collectCoverageFrom: [
        '<rootDir>/**/*.{js,jsx}',
        '!**/__mocks__/**'
    ]
};
