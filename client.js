const StaticServer = require('static-server');

const server = new StaticServer({
    rootPath: './dist',
    port: process.env.PORT || 8080
});

server.start(function () {
    console.log('Static server listening to', server.port);
});
