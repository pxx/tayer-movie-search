# Search movies
## Time spent — 20 hours approximately

## How to deal with it
### Easy run
```sh
$ git clone git@bitbucket.org:pxx/tayer-movie-search.git
$ yarn install
$ yarn dock:build
$ yarn dock
```
And then open [https://localhost:8080](https://localhost:8080) in your browser

### Development
In order to support development of a different parts of application there are some docker commands for partial running.
- `yarn dock:redis` - starts only redis container
- `yarn dock:api` - starts only api container
- `yarn dock:server` - starts api and redis containers
- `yarn start` - starts client in a watched dev mode
- `yarn client` - serves client
- `yarn server` - serves api
### Testing
Testing is performed using Jest + Enzyme.
Not all functionality is covered tests — I've chosen hardest module of every type. All others could be tested in a similar way and easier.

There are 2 commands to test application separately:
```sh
$ yarn test:client
$ yarn test:server
```
## Cool things used
- [Milligram](https://milligram.io/) - a minimalistic CSS framework that easily can be used as a base
- [Jest](https://jestjs.io/) - A great all-in-one framework for testing. Included Enzyme to easily test React components.
- [Axios](https://github.com/axios/axios) - Cool Promise based HTTP client for the browser and node.js.
- [react-debounce-input](https://www.npmjs.com/package/react-debounce-input) - input with debounced `onChange`.
- [react-on-screen](https://github.com/fkhadra/react-on-screen) - wrapper to check if react component is visible on a screen.
- [Restify](http://restify.com/) - A Node.js web service framework optimized for building semantically correct RESTful web services.
- [Redis](https://redis.io/) - great store for caches.
- [Docker Compose](https://docs.docker.com/compose/) - Easy tool for defining and running multi-container Docker applications.

## Things to be changed on production
- Use HTTPS
- Add nginx before api to improve performance
- Add service to restart falling services, e.g. `pm2`
- Set password for redis
- Use some logging system

## What could be improved
- gzip all responses
- Use some ORM to make api code more solid and to easily tie togeter 3rd party calls and caching
- Use better keys for redis
- Add accessibility aria- attributes for people with special needs
- Add schema.org attributes to results
- Make requests queue for /api/cache/request. Idle it if some of main requests is in progress
- Use eslint + husky for git pre-commit hooks
- Use TypeScript
- Check and sanitize input to prevent XSS-attacks if needed
- Cover all code with tests

