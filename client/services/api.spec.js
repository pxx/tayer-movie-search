import mockAxios from 'axios';
import { search } from './api';
import config from '../config';

describe('search()', () => {
    test('Should run axios.get() with params and return expected result', async () => {
        let response;
        const errorFn = jest.fn();

        mockAxios.get.mockImplementationOnce(() =>
            Promise.resolve({
                data: [{ title: 'sometitle', poster: 'http://url' }]
            })
        );

        try {
            response = await search('que');
        } catch (error) {
            errorFn();
        }

        expect(response).toEqual([{ title: 'sometitle', poster: 'http://url' }]);
        expect(mockAxios.get).toHaveBeenCalledWith(`${config.api.baseUrl}/search`, { params: { keyword: 'que' } });
        expect(errorFn).not.toHaveBeenCalled();
    });

    test('Should catch in case of error', async () => {
        let response;
        const errorFn = jest.fn();

        mockAxios.get.mockImplementationOnce(() =>
            Promise.reject('error')
        );

        try {
            response = await search('que');
        } catch (error) {
            errorFn();
        }

        expect(response).toBeUndefined();
        expect(errorFn).toHaveBeenCalled();
    });
});
