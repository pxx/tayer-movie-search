import axios from 'axios';

import config from '../config';

export async function search(query) {
    try {
        const movies = await axios.get(`${config.api.baseUrl}/search`, { params: { keyword: query } });
        return movies.data;
    } catch(error) {
        throw new Error(error);
    }
}
