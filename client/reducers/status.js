import { FETCH_MOVIES_FAILED, FETCH_MOVIES_STARTED, FETCH_MOVIES_SUCCEEDED } from '../actions/movies';
import { RESET_STATUS } from '../actions/status';

const defaultState = {
    code: 'idle',
    error: null
};

export default (state = defaultState, action) => {
    switch (action.type) {
        case FETCH_MOVIES_STARTED:
            return Object.assign({}, state, {
                code: 'loading',
                error: null
            });
        case RESET_STATUS:
        case FETCH_MOVIES_SUCCEEDED:
            return Object.assign({}, defaultState);
        case FETCH_MOVIES_FAILED:
            return Object.assign({}, state, {
                code: 'error',
                error: action.payload
            });
        default:
            return state;
    }
};
