import statusReducer from './status';
import { FETCH_MOVIES_FAILED, FETCH_MOVIES_STARTED, FETCH_MOVIES_SUCCEEDED } from '../actions/movies';
import { RESET_STATUS } from '../actions/status';

describe('status reducer', () => {
    const idleState = { code: 'idle', error: null };
    const loadingState = { code: 'loading', error: null };
    const errorState = { code: 'error' };

    test('Should return initial state', () => {
        expect(statusReducer(undefined, {}))
            .toEqual(idleState);
    });

    test('Should return state according to action', () => {
        expect(statusReducer(undefined, { type: FETCH_MOVIES_STARTED }))
            .toEqual(loadingState);
        expect(statusReducer(undefined, { type: RESET_STATUS }))
            .toEqual(idleState);
        expect(statusReducer(undefined, { type: FETCH_MOVIES_SUCCEEDED }))
            .toEqual(idleState);
        expect(statusReducer(undefined, { type: FETCH_MOVIES_FAILED, payload: 'some error', error: true }))
            .toEqual(Object.assign({}, errorState, { error: 'some error' }));
    });
});
