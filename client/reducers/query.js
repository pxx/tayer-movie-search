import { QUERY_SET_VALUE } from '../actions/query';

export default (state = '', action) => {
    switch(action.type) {
        case QUERY_SET_VALUE:
            return action.payload;
        default:
            return state;
    }
};
