import { EMPTY_MOVIES, FETCH_MOVIES_FAILED, FETCH_MOVIES_STARTED, FETCH_MOVIES_SUCCEEDED } from '../actions/movies';

export default (state = null, action) => {
    switch (action.type) {
        case EMPTY_MOVIES:
        case FETCH_MOVIES_STARTED:
        case FETCH_MOVIES_FAILED:
            return null;
        case FETCH_MOVIES_SUCCEEDED:
            return action.payload;
        default:
            return state;
    }
};
