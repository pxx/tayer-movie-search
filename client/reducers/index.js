import { combineReducers } from 'redux';

import query from './query';
import status from './status';
import movies from './movies';
import logger from './logger';

export default combineReducers({
    query,
    status,
    movies,
    logger
});
