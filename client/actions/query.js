import { fetchMovies } from './movies';

export const QUERY_SET_VALUE = 'QUERY_SET_VALUE';

export function setQueryValue(value) {
    return (dispatch) => {
        dispatch({ type: QUERY_SET_VALUE, payload: value });
        dispatch(fetchMovies());
    };
}
