export const RESET_STATUS = 'RESET_STATUS';

export function resetStatus() {
    return { type: RESET_STATUS };
}
