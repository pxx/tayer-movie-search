export const FETCH_MOVIES_STARTED = 'FETCH_MOVIES_STARTED';
export const FETCH_MOVIES_SUCCEEDED = 'FETCH_MOVIES_SUCCEEDED';
export const FETCH_MOVIES_FAILED = 'FETCH_MOVIES_FAILED';

export const EMPTY_MOVIES = 'EMPTY_MOVIES';

import { search } from '../services/api';

export function fetchMovies() {
    return (dispatch, getState) => {
        const { query } = getState();

        dispatch({ type: FETCH_MOVIES_STARTED });
        return search(query)
            .then((results) => {
                dispatch({ type: FETCH_MOVIES_SUCCEEDED, payload: results });
            })
            .catch((error) => {
                dispatch({ type: FETCH_MOVIES_FAILED, payload: error, error: true });
            });
    };
}

export function emptyMoviesList() {
    return { type: EMPTY_MOVIES };
}
