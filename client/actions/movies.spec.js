import configureMockStore from 'redux-mock-store';
import thunk from 'redux-thunk';
import mockAxios from 'axios';

import {
    FETCH_MOVIES_STARTED,
    FETCH_MOVIES_SUCCEEDED,
    EMPTY_MOVIES,
    FETCH_MOVIES_FAILED,
    fetchMovies,
    emptyMoviesList
} from './movies';

const mockStore = configureMockStore([thunk]);

describe('actions/movies.js', () => {
    describe('fetchMovies()', () => {
        test('Should create FETCH_MOVIES_SUCCEEDED when fetching has been done', () => {
            const payload = [{ title: 'sometitle', poster: 'http://url' }];

            mockAxios.get.mockImplementationOnce(() =>
                Promise.resolve({
                    data: payload
                })
            );

            const store = mockStore({ movies: [], query: 'que' });

            const expectedActions = [
                { type: FETCH_MOVIES_STARTED },
                { type: FETCH_MOVIES_SUCCEEDED, payload }
            ];

            return store.dispatch(fetchMovies()).then(() => {
                expect(store.getActions()).toEqual(expectedActions);
            });
        });

        test('Should create FETCH_MOVIES_FAILED when fetching failed', () => {
            mockAxios.get.mockImplementationOnce(() =>
                Promise.reject('network error')
            );

            const store = mockStore({ movies: [], query: 'que' });

            const expectedActions = [
                { type: FETCH_MOVIES_STARTED },
                { type: FETCH_MOVIES_FAILED, payload: 'network error', error: true }
            ];

            return store.dispatch(fetchMovies()).catch(() => {
                expect(store.getActions()).toEqual(expectedActions);
            });
        });
    });

    describe('emptyMoviesList()', () => {
        test('Create EMPTY_MOVIES to empty list of movies', () => {
            const expectedAction = { type: EMPTY_MOVIES };
            expect(emptyMoviesList()).toEqual(expectedAction);
        });
    });
});
