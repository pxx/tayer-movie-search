export default {
    api: {
        baseUrl: 'http://localhost:9000/api'
    },
    search: {
        minLength: 3,
        debounce: 300
    }
};
