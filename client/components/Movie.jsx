import React from 'react';
import TrackVisibility from 'react-on-screen';

export default class Movie extends React.Component {
    constructor(props) {
        super(props);

        this.onImageLoaded = this.onImageLoaded.bind(this);
        this.imageContainerRef = React.createRef();
    }

    trackVisibility() {
        console.log(`"${this.props.movie.title}" is visible`);
    }

    onImageLoaded(event) {
        this.imageContainerRef.current.style.minHeight = 'auto'
    }

    render() {
        const image = (
            <img className="movies-list__image"
                 src={this.props.movie.poster}
                 alt={this.props.movie.title}
                 onLoad={this.onImageLoaded}
            />
        );
        const noImage = <div className="movies-list__no-poster">No poster</div>;

        const contents = <>
            <div className="movie-list__image-container" ref={this.imageContainerRef}>
                {this.props.movie.poster ? image : noImage}
            </div>
            <h5 className="title movies-list__title">{this.props.movie.title}</h5>
        </>;

        return (
            <li className="movies-list__item">
                <TrackVisibility partialVisibility once>
                    {({ isVisible }) => {
                        if (isVisible) {
                            this.trackVisibility();
                        }

                        return contents;
                    }}
                </TrackVisibility>
            </li>
        );
    }
}
