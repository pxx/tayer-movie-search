import React from 'react';

import SearchForm from './SearchForm';
import SearchStatus from './SearchStatus';
import MoviesList from './MoviesList';

export default () => (
    <div className="wrapper">
        <h1 className="title">Movie Search</h1>

        <SearchForm/>
        <SearchStatus/>
        <MoviesList/>
    </div>
);
