import React from 'react';
import { connect } from 'react-redux';

import './MovieList.scss';

import Movie from './Movie';

@connect(mapStateToProps, null)
export default class MoviesList extends React.Component {
    composeMoviesList() {
        return this.props.movies.map((movie, index) => <Movie movie={movie} key={index} />);
    }

    render() {
        if (!this.props.movies) {
            return null;
        } else {
            const results = this.props.movies.length
                ? <ul className="movies-list">{this.composeMoviesList()}</ul>
                : <p className="results__no-results">No results found</p>;
            return (
                <div className="results">
                    <h2 className="title">Search Results for "{this.props.query}"</h2>
                    {results}
                </div>
            );
        }
    }
}

function mapStateToProps(state) {
    return {
        movies: state.movies,
        query: state.query
    };
}
