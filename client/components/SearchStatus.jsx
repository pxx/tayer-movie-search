import React from 'react';
import { connect } from 'react-redux';

import './SearchStatus.scss';

@connect(mapStateToProps, null)
export default class SearchStatus extends React.Component {
    render() {
        const status = this.props.status;
        switch (status.code) {
            case 'loading':
                return <div className="status status--loading">Loading...</div>;
            case 'error':
                return <div className="status status--error">Failed to fetch movies from server</div>;
            case 'idle':
            default:
                return null;
        }
    }
}

function mapStateToProps(state) {
    return {
        status: state.status
    };
}
