import React from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { DebounceInput } from 'react-debounce-input';

import config from '../config';

import { setQueryValue } from '../actions/query';
import { emptyMoviesList } from '../actions/movies';
import { resetStatus } from '../actions/status';

export class SearchForm extends React.Component {
    constructor(props) {
        super(props);

        this.onChangeQuery = this.onChangeQuery.bind(this);
    }

    onChangeQuery(event) {
        const query = event.target.value;

        if (query.length < config.search.minLength) {
            this.props.resetStatus();
            this.props.emptyMoviesList();
        } else {
            this.props.setQueryValue(query);
        }
    }

    render() {
        return (
            <div className="form">
                <form>
                    <DebounceInput
                        className="form-input"
                        debounceTimeout={config.search.debounce}
                        onChange={this.onChangeQuery}
                        placeholder="Movie name"
                    />
                </form>
            </div>
        );
    }
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators({
        setQueryValue,
        emptyMoviesList,
        resetStatus
    }, dispatch);
}

export default connect(null, mapDispatchToProps)(SearchForm);
