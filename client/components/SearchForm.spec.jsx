import React from 'react';
import { mount, configure } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';

import config from '../config';
import { SearchForm } from './SearchForm';

configure({ adapter: new Adapter() });


function setup() {
    const props = {
        resetStatus: jest.fn(),
        emptyMoviesList: jest.fn(),
        setQueryValue: jest.fn()
    };

    const enzymeWrapper = mount(<SearchForm {...props} />);

    return {
        props,
        enzymeWrapper
    };
}

describe('SearchForm component', () => {
    test('Should render self and children', () => {
        const { enzymeWrapper } = setup();

        expect(enzymeWrapper.find('div.form').length).toBe(1);

        const input = enzymeWrapper.find('input');
        expect(input.length).toBe(1);
        expect(input.props().value).toBe('');
        expect(input.props().placeholder).toBe('Movie name');
        expect(input.hasClass('form-input')).toBeTruthy();
    });

    test('Should handle change after debounce time', (done) => {
        const spy = jest.spyOn(SearchForm.prototype, 'onChangeQuery');

        const { enzymeWrapper } = setup();

        const input = enzymeWrapper.find('input').at(0);
        input.simulate('change', { target: { value: '123' } });

        setTimeout(() => {
            expect(spy).not.toHaveBeenCalled();
        }, config.search.debounce - 5);
        setTimeout(() => {
            expect(spy).toHaveBeenCalled();
            done();
        }, config.search.debounce + 5);
    });

    test('Should reset app to initial state if query length < config.search.minLength', (done) => {
        if (config.search.minLength > 0) {
            const { enzymeWrapper, props } = setup();
            const query = Array(config.search.minLength - 1).fill('1').join('');
            const input = enzymeWrapper.find('input').at(0);
            input.simulate('change', { target: { value: query } });

            setTimeout(() => {
                expect(props.resetStatus).toHaveBeenCalled();
                expect(props.emptyMoviesList).toHaveBeenCalled();
                done();
            }, config.search.debounce + 5);
        } else {
            done();
        }
    });

    test('Should set query value in store if query length >= config.search.minLength', (done) => {
        const { enzymeWrapper, props } = setup();
        const query = Array(config.search.minLength).fill('1').join('');
        const input = enzymeWrapper.find('input').at(0);
        input.simulate('change', { target: { value: query } });

        setTimeout(() => {
            expect(props.setQueryValue).toHaveBeenCalled();
            done();
        }, config.search.debounce + 5);
    });
});
